SET @TEXT_ID := 50000;
DELETE FROM `npc_text` WHERE `ID` IN  (@TEXT_ID,@TEXT_ID+1);

INSERT INTO `npc_text` (`ID`, `text0_0`) VALUES
(@TEXT_ID, '幻化允許你改變護甲和武器的外觀。\r\n用於幻化的物品將會不能交易，及與你綁定。\r\n更新選單會更新外觀和價格。\r\n\r\n並非所有物器都可以互相幻化。\r\n限制包括但不限於：\r\n只有護甲和武器可以幻化\r\n槍、弓和弩可以互相幻化\r\n魚竿不能幻化\r\n您必須能夠裝備及使用兩個物品。\r\n\r\n只要你擁有它們，幻化就會留在你的物品上。\r\n如果您將該使用於幻化的物器放入公會銀行或將其郵寄給其他人，幻化將會失效。\r\n\r\n您也可以使用移除幻化功能免費還原物品原來的外觀。'),
(@TEXT_ID+1, '您可以保存自己的幻化套裝。\r\n\r\n要保存，首先必須對配備的物品進行幻化。\r\n然後，到設置管理選單並轉到保存設置選單時，\r\n顯示您已經幻化的所有項目，以便您查看正在保存的內容。\r\n如果您認為該設置沒問題，您可以點擊以保存該設置並根據你的喜好命名。\r\n\r\n要使用套裝，可以點擊設置管理選單中已保存的套裝，然後選擇使用套裝。\r\n如果該套裝已經於其他物品中使用幻化，舊的幻化則會遺失。\r\n請留意，在使用正常幻化的套裝時，適用相同的幻化限制。\r\n\r\n要刪除一組幻化套裝，您可以轉到設置選單並選擇刪除設置。');

SET @STRING_ENTRY := 11100;
DELETE FROM `trinity_string` WHERE `entry` IN  (@STRING_ENTRY+0,@STRING_ENTRY+1,@STRING_ENTRY+2,@STRING_ENTRY+3,@STRING_ENTRY+4,@STRING_ENTRY+5,@STRING_ENTRY+6,@STRING_ENTRY+7,@STRING_ENTRY+8,@STRING_ENTRY+9,@STRING_ENTRY+10);

INSERT INTO `trinity_string` (`entry`, `content_default`) VALUES
(@STRING_ENTRY+0, '物品已幻化過了'),
(@STRING_ENTRY+1, '裝備欄是空的'),
(@STRING_ENTRY+2, '選擇的物品無效'),
(@STRING_ENTRY+3, '來源物品無效'),
(@STRING_ENTRY+4, '目標物品無效'),
(@STRING_ENTRY+5, '選擇的物品無效'),
(@STRING_ENTRY+6, '金錢不足'),
(@STRING_ENTRY+7, '你沒有足夠的Token'),
(@STRING_ENTRY+8, '幻化已經解除'),
(@STRING_ENTRY+9, '此物品不能幻化'),
(@STRING_ENTRY+10, '不適用的名稱');
